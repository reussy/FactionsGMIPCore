package com.reussy.gmip;

import com.reussy.gmip.events.BlockPlaceListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class FactionsCore extends JavaPlugin {

    @Override
    public void onEnable() {

        Bukkit.getConsoleSender().sendMessage(("----------------------------"));
        Bukkit.getConsoleSender().sendMessage(("FactionsGMIPCore - Enabled"));
        Bukkit.getConsoleSender().sendMessage(("by reussy - GMIP"));
        Bukkit.getConsoleSender().sendMessage((""));
        generateConfig();
        onHooks();
        Bukkit.getPluginManager().registerEvents(new BlockPlaceListener(), this);
        Bukkit.getConsoleSender().sendMessage(("----------------------------"));
    }

    @Override
    public void onDisable() {

    }

    public void onHooks() {

        PluginManager pluginManager = Bukkit.getPluginManager();

        if (pluginManager.getPlugin("Factions") != null) {

            Bukkit.getConsoleSender().sendMessage(("Factions - Hooked"));
        } else {
            Bukkit.getConsoleSender().sendMessage(("Factions - Not found!"));
            pluginManager.disablePlugin(this);
        }
    }

    public void generateConfig() {

        File configFile = new File(this.getDataFolder(), "config.yml");
        if (!configFile.exists()) {

            Bukkit.getConsoleSender().sendMessage(("Creating config file..."));
            this.saveResource("config.yml", false);
        }
    }

    public String setColorText(String text) {

        return ChatColor.translateAlternateColorCodes('&', text);
    }
}
