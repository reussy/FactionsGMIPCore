package com.reussy.gmip.events;

import com.massivecraft.factions.*;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

public class BlockPlaceListener implements Listener {

    @EventHandler
    public void onPlaceCollector(BlockPlaceEvent e){

        Faction faction;
        Player player = e.getPlayer();
        FPlayer fPlayer = FPlayers.getInstance().getByPlayer(player);
        FLocation fLocation = new FLocation(player.getLocation());
        faction = Board.getInstance().getFactionAt(fLocation);
        Block blockPlaced = e.getBlockPlaced();
        Block block = e.getBlock();
        ItemStack item = e.getItemInHand();

        if (player.isOp() || fPlayer.isAdminBypassing() || player.hasPermission("*")) return;

        if (blockPlaced.getType() == Material.BEACON || block.getType() == Material.BEACON) {
            if (item.getItemMeta().hasDisplayName()) {
                if (!fPlayer.getFaction().equals(faction)) e.setCancelled(true);

                if (!blockPlaced.getChunk().equals(fLocation.getChunk())) e.setCancelled(true);

                if (!fPlayer.hasFaction()) e.setCancelled(true);
            }
        }
    }
}
